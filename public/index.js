// for client side we use IIFE to execute the application
(function() {
    console.log("I'm on the client side");
    // everything inside this function is our application
    // Create an instance of Angular application
    //1st param: app name, the second is the app dependencies
    var FirstApp = angular.module("FirstApp",[]);

    var reverse = function (s){
        return s.split("").reverse().join("");
    }

    var FirstCtrl = function() {
        // hold a reference of this controller so that when 'this' changes we are 
        //still referencing the controller
        var firstCtrl = this;

        //define first model of thes controller/vifew model vm
        firstCtrl.myText = "hello, world";

        //define another model
        firstCtrl.reverseMyText = "";

        //define an event for the click button
        firstCtrl.reverseText = function () {
            firstCtrl.reverseMyText = reverse(firstCtrl.myText);
            // console.log("test " + firstCtrl.reverseMyText);
            // console.log(firstCtrl.myText);
        }


        //define an event for the click button
        firstCtrl.clearText = function () {
            firstCtrl.reverseMyText =   "";
            // console.log("test " + firstCtrl.reverseMyText);
            // console.log(firstCtrl.myText);
        }


    }

    //Define a controller call FirstCtrl -
    FirstApp.controller("FirstCtrl",[ FirstCtrl ]);

})();