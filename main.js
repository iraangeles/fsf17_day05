// Angular project
var express = require('express');
var path = require("path");
var app = express();


// Configure port
app.set("port",parseInt(process.argv[2]) || process.env.APP_PORT || 3000);

//Configure routes
app.use(express.static(path.join(__dirname,"public")));

//Set bower route
app.use("/libs",express.static(path.join(__dirname,"bower_components")));



app.listen(app.get("port"),function(){
    console.log("Application started at %s on port %d", new Date(),app.get("port"));

}
);


